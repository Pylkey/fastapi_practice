from fastapi import FastAPI, Request
# from fastapi.exceptions import HTTPException
from fastapi.responses import JSONResponse
# from fastapi.responses import PlainTextResponse
from exceptions import StoryExeption
from routers import blog_get, blog_post, user, article, product, file
from auth import authentication
from db import models
from db.database import engine
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()
app.include_router(authentication.router)
app.include_router(file.router)
app.include_router(blog_get.router)
app.include_router(blog_post.router)
app.include_router(user.router)
app.include_router(article.router)
app.include_router(product.router)

@app.get('/hello')
def index():
  return {'message': 'Hello world!'}

@app.exception_handler(StoryExeption)
def story_exception_handler(requst: Request, exc: StoryExeption):
  return JSONResponse(
    status_code=418,
    content={'detail': exc.name}
  )

# @app.exception_handler(HTTPException)
# def custom_handler(requst: Request, exc: StoryExeption):
#   return PlainTextResponse(str(exc, status_code=400))

origins = [
  'http://localhist:3000'
]

models.Base.metadata.create_all(engine)

app.add_middleware(
  CORSMiddleware,
  allow_origins=origins,
  allow_credentials=True,
  allow_methods = ['*'],
  allow_headers = ['*']
)