from fastapi import APIRouter, File, UploadFile
import shutil, os.path
from fastapi.responses import FileResponse


router = APIRouter(
    prefix='/file',
    tags=['file']
)


@router.post('/file')
def get_file(file: bytes = File(...)):
    content = file.decode('utf8')
    lines = content.split('\n')
    return {'lines': lines}


@router.post('/uploadfile')
def get_upload_file(upload_file: UploadFile = File(...)):
    path = os.path.join('routers','uploaded_file',upload_file.filename)
    with open(path, 'wb+') as buffer:
        # files with same filename be overwrite
        shutil.copyfileobj(upload_file.file, buffer)
    return{
        'filename': path,
        'type': upload_file.content_type
         
    }

@router.get('/download/{name}', response_class = FileResponse)
def get_file(name: str):
    path = os.path.join('routers','uploaded_file', name)
    return path