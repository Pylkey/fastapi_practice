from typing import Dict, List, Optional
from fastapi import APIRouter, Query, Body, Path
from pydantic import BaseModel


router = APIRouter(
    prefix = '/blog',
    tags=['blog']
)

class Image(BaseModel):
    url: str
    alias: str
class BlogModel(BaseModel):
    title: str
    content: str
    num_comment: int
    published: Optional[bool]
    tags: List[str] = []
    metadata: Dict[str, str] = {'key1':'val2'}
    image:Optional[Image] = None

@router.post('/new/{id}')
def create_blog(blog: BlogModel, id: int, version: int = 1):
    return {
        'id': id,
        'data': blog,
        'version':version
    }


@router.post('/new/{id}/comment/{comment_id}')
def create_comment(
        blog: BlogModel, id: int, 
        comment_title: int = Query(None,
            title='Id комментария',
            description='Описание для comment_title',
            alias='CommentTitle',
            deprecated=True
        ),
        content: str = Body(..., # validator
            min_length=10,
            max_length=50,
            regex='^[а-я\s]*$'
        ),
        params: Optional[List[str]] = Query(['1.0', '1.1', '1.2']), # Multiply parameters
        comment_id: int = Path(None, gt=5, le=10)
    ):
    return {
        'blog': blog,
        'id': id,
        'comment_title': comment_title,
        'content': content,
        'version': params,
        'comment_id': comment_id
    }

def requared_functionality():
    return {'message': 'Учим FastAPI важное'}