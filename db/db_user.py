from sqlalchemy.orm.session import Session
from db.hash import Hash
from schema import UserBase
from db.models import DbUsers
from fastapi import HTTPException, status


def create_user(db: Session, request: UserBase):
    new_user = DbUsers(
        username = request.username,
        email = request.email,
        password = Hash.bcrypt(request.password)
    )
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user

def get_all_users(db: Session):
    return db.query(DbUsers).all()


def get_user_by_username(db: Session, username: str):
    user = db.query(DbUsers).filter(DbUsers.username == username).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
        detail=f'Пользователь {username} не найден')
    return user

def update_user(db: Session, id: int, request: UserBase):
    user = db.query(DbUsers).filter(DbUsers.id == id)
    if not user.first():
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
        detail=f'Пользователь с id {id} не найден')
    user.update({
        DbUsers.username: request.username,
        DbUsers.email: request.email,
        DbUsers.password: Hash.bcrypt(request.password)
    })
    db.commit()
    return 'ok'

def delete_user(db: Session, id: int):
    user = db.query(DbUsers).filter(DbUsers.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
        detail=f'Пользователь с id {id} не найден')
    db.delete(user)
    db.commit()
    return 'ok'