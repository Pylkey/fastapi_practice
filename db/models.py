from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey
from db.database import Base
import sqlalchemy as sa


class DbUsers(Base):
    __tablename__ = 'users'
    id = sa.Column(sa.Integer, primary_key=True, index=True)
    username = sa.Column(sa.String)
    email = sa.Column(sa.String)
    password = sa.Column(sa.String)
    items = relationship('DBarticle', back_populates='user')

class DBarticle(Base):
    __tablename__ = 'articles'
    id = sa.Column(sa.Integer, primary_key=True, index=True)
    title = sa.Column(sa.String)
    content = sa.Column(sa.String)
    published = sa.Column(sa.Boolean)
    user_id = sa.Column(sa.Integer, ForeignKey('users.id'))
    user = relationship('DbUsers', back_populates='items')