from sqlalchemy.orm.session import Session
from db.models import DBarticle
from exceptions import StoryExeption
from schema import ArticleBase
from fastapi import HTTPException, status


def create_article(db: Session, request: ArticleBase):
    if request.content.startswith('Однажды'):
        raise StoryExeption('Не надо историй')
    new_article = DBarticle(
        title = request.title,
        content = request.content,
        published = request.published,
        user_id = request.creator_id
    )
    db.add(new_article)
    db.commit()
    db.refresh(new_article)
    return new_article


def get_article(db: Session, id: int):
    article = db.query(DBarticle).filter(DBarticle.id == id).first()
    if not article:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
        detail=f'Статья с id {id} не найдена')
    return article